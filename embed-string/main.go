package main

import (
	// note the _ which is requires since it's not being called
	// directly anywhere in this case, and is just a build directive
	_ "embed"
	"fmt"
)

// note that hello can't be defined as a const which means it could
// be overwritten by your application at runtime
//go:embed hello.txt
var hello string

func main() {
	fmt.Printf("%s\n", hello)
}
