# Embed String
The idea of this example is that it will embed the contents of a [file](hello.txt) into a string variable.   

## Building/Running
### Building
If you prefer to build the binary you can run `go build` from within the embed-string directory, and an embed-string binary will be created.  Once created you can run it like you would run any other command line tool and see that it prints "hello world" and exits.

### Running
If you prefer to run it without outputting the binary, you can run `go run main.go` from within the embed-string directory, then see that it prints out "hello world" and exits.