package main

import (
	"embed"
	"io/fs"
	"log"
	"net/http"
)

// This will embed anything under webapp in your project
//go:embed webapp/*
var files embed.FS

func main() {
	println("server listening on port 8080")
	webapp, err := fs.Sub(files, "webapp")
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Fatal(http.ListenAndServe(":8080", http.FileServer(http.FS(webapp))))
}
