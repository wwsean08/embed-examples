# Embed Filesystem
The idea of this example is that it will embed a small hello world website into the binary and serve the pages directly from the filesystem within the binary.

## Building/Running
### Building
If you prefer to build the binary you can run `go build` from within the embed-fs directory, and an embed-fs binary will be created.  Once created you can run it like you would run any other command line tool, and open http://localhost:8080 in your browser.

### Running
If you prefer to run it without outputting the binary, you can run `go run main.go` from within the embed-fs directory, then head to http://localhost:8080 in your browser.