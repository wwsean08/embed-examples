package main

import (
	"embed"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
)

// This will embed anything under webapp in your project
//go:embed webapp/*
var files embed.FS

const assetRoot = "./tmp"

func main() {
	err := updateFS()
	if err != nil {
		log.Fatal(err.Error())
	}
	webapp := os.DirFS(assetRoot)
	webapp, err = fs.Sub(files, "webapp")
	if err != nil {
		log.Fatal(err)
	}
	println("server listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", http.FileServer(http.FS(webapp))))
}

func updateFS() error {
	return fs.WalkDir(files, ".", walk)
}

func walk(path string, d fs.DirEntry, _ error) error {
	if path == "." {
		return nil
	}
	newPath := fmt.Sprintf("%s/%s", assetRoot, path)

	if d.IsDir() {
		// if the directory doesn't exist, create it
		if _, err := os.Stat(newPath); os.IsNotExist(err) {
			return os.MkdirAll(newPath, 0700)
		}
	} else {
		fInfo, err := os.Stat(newPath)
		// file doesn't exist, create it
		if os.IsNotExist(err) {
			file, err := files.Open(path)
			if err != nil {
				return nil
			}
			data, err := files.ReadFile(path)
			if err != nil {
				return err
			}
			fStat, err := file.Stat()
			if err != nil {
				return err
			}

			return os.WriteFile(newPath, data, fStat.Mode())
		} else {
			// file exists, update it if this version is newer (based on last modified time)
			file, err := files.Open(path)
			if err != nil {
				return nil
			}
			fStat, err := file.Stat()
			if err != nil {
				return err
			}
			if fStat.ModTime().After(fInfo.ModTime()) {
				data, err := files.ReadFile(path)
				if err != nil {
					return err
				}

				return os.WriteFile(newPath, data, fStat.Mode())
			}
		}
	}
	return nil
}
