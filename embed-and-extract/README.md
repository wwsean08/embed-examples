# Embed and Extract
The idea of this example is that it will embed a small hello world website into the binary, then on startup extract them under a tmp directory.  If the files don't exist, they are blindly written, if they do exist already on the filesystem, then the last modified time is checked, and if the file(s) within the binary were modified more recently, the files on the disk are overwritten.

## Building/Running
### Building
If you prefer to build the binary you can run `go build` from within the embed-and-extract directory, and an embed-and-extract binary will be created.  Once created you can run it like you would run any other command line tool, and open http://localhost:8080 in your browser.

### Running
If you prefer to run it without outputting the binary, you can run `go run main.go` from within the embed-and-extract directory, then head to http://localhost:8080 in your browser.