# Embed Examples

This code provides examples for how to use the `embed` package introduced in go 1.16.  The following examples are provided:
* [Embedding a files contents into a string](embed-string)
* [Embedding a directory structure and using it](embed-fs)
* [Embedding a directory structure and using it to manage the files on the local filesystem](embed-and-extract)

## License
This code is licensed under [CC-BY-SA-4.0](LICENSE) and was created for [https://golang.wtf](https://golang.wtf)